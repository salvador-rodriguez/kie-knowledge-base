//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.01.27 at 02:16:40 PM GMT 
//


package vmr.schema.v1_0.vmr.opencds.org;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the vmr.schema.v1_0.vmr.opencds.org package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Vmr_QNAME = new QName("org.opencds.vmr.v1_0.schema.vmr", "vmr");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: vmr.schema.v1_0.vmr.opencds.org
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists.Facilities }
     * 
     */
    public EvaluatedPerson.EntityLists.Facilities createEvaluatedPersonEntityListsFacilities() {
        return new EvaluatedPerson.EntityLists.Facilities();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SupplyEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SupplyEvents createEvaluatedPersonClinicalStatementsSupplyEvents() {
        return new EvaluatedPerson.ClinicalStatements.SupplyEvents();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ObservationProposals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ObservationProposals createEvaluatedPersonClinicalStatementsObservationProposals() {
        return new EvaluatedPerson.ClinicalStatements.ObservationProposals();
    }

    /**
     * Create an instance of {@link VMR.EvaluatedPersonRelationships }
     * 
     */
    public VMR.EvaluatedPersonRelationships createVMREvaluatedPersonRelationships() {
        return new VMR.EvaluatedPersonRelationships();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ObservationResults }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ObservationResults createEvaluatedPersonClinicalStatementsObservationResults() {
        return new EvaluatedPerson.ClinicalStatements.ObservationResults();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ScheduledAppointments }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ScheduledAppointments createEvaluatedPersonClinicalStatementsScheduledAppointments() {
        return new EvaluatedPerson.ClinicalStatements.ScheduledAppointments();
    }

    /**
     * Create an instance of {@link EntitySimple }
     * 
     */
    public EntitySimple createEntitySimple() {
        return new EntitySimple();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ScheduledProcedures }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ScheduledProcedures createEvaluatedPersonClinicalStatementsScheduledProcedures() {
        return new EvaluatedPerson.ClinicalStatements.ScheduledProcedures();
    }

    /**
     * Create an instance of {@link VMR }
     * 
     */
    public VMR createVMR() {
        return new VMR();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.EncounterEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.EncounterEvents createEvaluatedPersonClinicalStatementsEncounterEvents() {
        return new EvaluatedPerson.ClinicalStatements.EncounterEvents();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.UnconductedObservations }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.UnconductedObservations createEvaluatedPersonClinicalStatementsUnconductedObservations() {
        return new EvaluatedPerson.ClinicalStatements.UnconductedObservations();
    }

    /**
     * Create an instance of {@link GoalProposal }
     * 
     */
    public GoalProposal createGoalProposal() {
        return new GoalProposal();
    }

    /**
     * Create an instance of {@link RelatedEntity.Organization }
     * 
     */
    public RelatedEntity.Organization createRelatedEntityOrganization() {
        return new RelatedEntity.Organization();
    }

    /**
     * Create an instance of {@link SubstanceAdministrationProposal }
     * 
     */
    public SubstanceAdministrationProposal createSubstanceAdministrationProposal() {
        return new SubstanceAdministrationProposal();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SubstanceAdministrationEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SubstanceAdministrationEvents createEvaluatedPersonClinicalStatementsSubstanceAdministrationEvents() {
        return new EvaluatedPerson.ClinicalStatements.SubstanceAdministrationEvents();
    }

    /**
     * Create an instance of {@link FacilitySimple }
     * 
     */
    public FacilitySimple createFacilitySimple() {
        return new FacilitySimple();
    }

    /**
     * Create an instance of {@link SupplyEvent }
     * 
     */
    public SupplyEvent createSupplyEvent() {
        return new SupplyEvent();
    }

    /**
     * Create an instance of {@link ClinicalStatementRelationship }
     * 
     */
    public ClinicalStatementRelationship createClinicalStatementRelationship() {
        return new ClinicalStatementRelationship();
    }

    /**
     * Create an instance of {@link ObservationResult.ObservationValue }
     * 
     */
    public ObservationResult.ObservationValue createObservationResultObservationValue() {
        return new ObservationResult.ObservationValue();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.Facility }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.Facility createFacility() {
        return new vmr.schema.v1_0.vmr.opencds.org.Facility();
    }

    /**
     * Create an instance of {@link ObservationOrder }
     * 
     */
    public ObservationOrder createObservationOrder() {
        return new ObservationOrder();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.Person }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.Person createPerson() {
        return new vmr.schema.v1_0.vmr.opencds.org.Person();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.DeniedAdverseEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.DeniedAdverseEvents createEvaluatedPersonClinicalStatementsDeniedAdverseEvents() {
        return new EvaluatedPerson.ClinicalStatements.DeniedAdverseEvents();
    }

    /**
     * Create an instance of {@link SubstanceAdministrationOrder }
     * 
     */
    public SubstanceAdministrationOrder createSubstanceAdministrationOrder() {
        return new SubstanceAdministrationOrder();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.GoalProposals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.GoalProposals createEvaluatedPersonClinicalStatementsGoalProposals() {
        return new EvaluatedPerson.ClinicalStatements.GoalProposals();
    }

    /**
     * Create an instance of {@link RelatedEntity.Person }
     * 
     */
    public RelatedEntity.Person createRelatedEntityPerson() {
        return new RelatedEntity.Person();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists.AdministrableSubstances }
     * 
     */
    public EvaluatedPerson.EntityLists.AdministrableSubstances createEvaluatedPersonEntityListsAdministrableSubstances() {
        return new EvaluatedPerson.EntityLists.AdministrableSubstances();
    }

    /**
     * Create an instance of {@link ScheduledAppointment }
     * 
     */
    public ScheduledAppointment createScheduledAppointment() {
        return new ScheduledAppointment();
    }

    /**
     * Create an instance of {@link Problem }
     * 
     */
    public Problem createProblem() {
        return new Problem();
    }

    /**
     * Create an instance of {@link ProcedureEvent }
     * 
     */
    public ProcedureEvent createProcedureEvent() {
        return new ProcedureEvent();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SupplyOrders }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SupplyOrders createEvaluatedPersonClinicalStatementsSupplyOrders() {
        return new EvaluatedPerson.ClinicalStatements.SupplyOrders();
    }

    /**
     * Create an instance of {@link ObservationResult }
     * 
     */
    public ObservationResult createObservationResult() {
        return new ObservationResult();
    }

    /**
     * Create an instance of {@link RelatedEntity.Specimen }
     * 
     */
    public RelatedEntity.Specimen createRelatedEntitySpecimen() {
        return new RelatedEntity.Specimen();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.GoalBase.TargetGoalValue }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.GoalBase.TargetGoalValue createGoalBaseTargetGoalValue() {
        return new vmr.schema.v1_0.vmr.opencds.org.GoalBase.TargetGoalValue();
    }

    /**
     * Create an instance of {@link BodySite }
     * 
     */
    public BodySite createBodySite() {
        return new BodySite();
    }

    /**
     * Create an instance of {@link SubstanceAdministrationEvent }
     * 
     */
    public SubstanceAdministrationEvent createSubstanceAdministrationEvent() {
        return new SubstanceAdministrationEvent();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.Entity }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.Entity createEntity() {
        return new vmr.schema.v1_0.vmr.opencds.org.Entity();
    }

    /**
     * Create an instance of {@link EntityRelationship }
     * 
     */
    public EntityRelationship createEntityRelationship() {
        return new EntityRelationship();
    }

    /**
     * Create an instance of {@link AdministrableSubstanceSimple }
     * 
     */
    public AdministrableSubstanceSimple createAdministrableSubstanceSimple() {
        return new AdministrableSubstanceSimple();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityRelationships }
     * 
     */
    public EvaluatedPerson.EntityRelationships createEvaluatedPersonEntityRelationships() {
        return new EvaluatedPerson.EntityRelationships();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SubstanceAdministrationOrders }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SubstanceAdministrationOrders createEvaluatedPersonClinicalStatementsSubstanceAdministrationOrders() {
        return new EvaluatedPerson.ClinicalStatements.SubstanceAdministrationOrders();
    }

    /**
     * Create an instance of {@link DoseRestriction }
     * 
     */
    public DoseRestriction createDoseRestriction() {
        return new DoseRestriction();
    }

    /**
     * Create an instance of {@link SpecimenSimple }
     * 
     */
    public SpecimenSimple createSpecimenSimple() {
        return new SpecimenSimple();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists }
     * 
     */
    public EvaluatedPerson.EntityLists createEvaluatedPersonEntityLists() {
        return new EvaluatedPerson.EntityLists();
    }

    /**
     * Create an instance of {@link ProcedureProposal }
     * 
     */
    public ProcedureProposal createProcedureProposal() {
        return new ProcedureProposal();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists.Persons }
     * 
     */
    public EvaluatedPerson.EntityLists.Persons createEvaluatedPersonEntityListsPersons() {
        return new EvaluatedPerson.EntityLists.Persons();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ProcedureOrders }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ProcedureOrders createEvaluatedPersonClinicalStatementsProcedureOrders() {
        return new EvaluatedPerson.ClinicalStatements.ProcedureOrders();
    }

    /**
     * Create an instance of {@link Goal }
     * 
     */
    public Goal createGoal() {
        return new Goal();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatementRelationships }
     * 
     */
    public EvaluatedPerson.ClinicalStatementRelationships createEvaluatedPersonClinicalStatementRelationships() {
        return new EvaluatedPerson.ClinicalStatementRelationships();
    }

    /**
     * Create an instance of {@link MissedAppointment }
     * 
     */
    public MissedAppointment createMissedAppointment() {
        return new MissedAppointment();
    }

    /**
     * Create an instance of {@link RelatedEntity.Facility }
     * 
     */
    public RelatedEntity.Facility createRelatedEntityFacility() {
        return new RelatedEntity.Facility();
    }

    /**
     * Create an instance of {@link RelatedEntity.AdministrableSubstance }
     * 
     */
    public RelatedEntity.AdministrableSubstance createRelatedEntityAdministrableSubstance() {
        return new RelatedEntity.AdministrableSubstance();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.Problems }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.Problems createEvaluatedPersonClinicalStatementsProblems() {
        return new EvaluatedPerson.ClinicalStatements.Problems();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists.Entities }
     * 
     */
    public EvaluatedPerson.EntityLists.Entities createEvaluatedPersonEntityListsEntities() {
        return new EvaluatedPerson.EntityLists.Entities();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.Demographics }
     * 
     */
    public EvaluatedPerson.Demographics createEvaluatedPersonDemographics() {
        return new EvaluatedPerson.Demographics();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.Specimen }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.Specimen createSpecimen() {
        return new vmr.schema.v1_0.vmr.opencds.org.Specimen();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.AppointmentProposals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.AppointmentProposals createEvaluatedPersonClinicalStatementsAppointmentProposals() {
        return new EvaluatedPerson.ClinicalStatements.AppointmentProposals();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.MissedAppointments }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.MissedAppointments createEvaluatedPersonClinicalStatementsMissedAppointments() {
        return new EvaluatedPerson.ClinicalStatements.MissedAppointments();
    }

    /**
     * Create an instance of {@link AppointmentRequest }
     * 
     */
    public AppointmentRequest createAppointmentRequest() {
        return new AppointmentRequest();
    }

    /**
     * Create an instance of {@link VMR.OtherEvaluatedPersons }
     * 
     */
    public VMR.OtherEvaluatedPersons createVMROtherEvaluatedPersons() {
        return new VMR.OtherEvaluatedPersons();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatementEntityInRoleRelationships }
     * 
     */
    public EvaluatedPerson.ClinicalStatementEntityInRoleRelationships createEvaluatedPersonClinicalStatementEntityInRoleRelationships() {
        return new EvaluatedPerson.ClinicalStatementEntityInRoleRelationships();
    }

    /**
     * Create an instance of {@link EncounterEvent }
     * 
     */
    public EncounterEvent createEncounterEvent() {
        return new EncounterEvent();
    }

    /**
     * Create an instance of {@link UnconductedObservation }
     * 
     */
    public UnconductedObservation createUnconductedObservation() {
        return new UnconductedObservation();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ObservationOrders }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ObservationOrders createEvaluatedPersonClinicalStatementsObservationOrders() {
        return new EvaluatedPerson.ClinicalStatements.ObservationOrders();
    }

    /**
     * Create an instance of {@link DeniedProblem }
     * 
     */
    public DeniedProblem createDeniedProblem() {
        return new DeniedProblem();
    }

    /**
     * Create an instance of {@link PersonSimple }
     * 
     */
    public PersonSimple createPersonSimple() {
        return new PersonSimple();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists.Specimens }
     * 
     */
    public EvaluatedPerson.EntityLists.Specimens createEvaluatedPersonEntityListsSpecimens() {
        return new EvaluatedPerson.EntityLists.Specimens();
    }

    /**
     * Create an instance of {@link RelatedClinicalStatement }
     * 
     */
    public RelatedClinicalStatement createRelatedClinicalStatement() {
        return new RelatedClinicalStatement();
    }

    /**
     * Create an instance of {@link UndeliveredSupply }
     * 
     */
    public UndeliveredSupply createUndeliveredSupply() {
        return new UndeliveredSupply();
    }

    /**
     * Create an instance of {@link ProcedureOrder }
     * 
     */
    public ProcedureOrder createProcedureOrder() {
        return new ProcedureOrder();
    }

    /**
     * Create an instance of {@link ScheduledProcedure }
     * 
     */
    public ScheduledProcedure createScheduledProcedure() {
        return new ScheduledProcedure();
    }

    /**
     * Create an instance of {@link RelatedEntity }
     * 
     */
    public RelatedEntity createRelatedEntity() {
        return new RelatedEntity();
    }

    /**
     * Create an instance of {@link SubstanceDispensationEvent }
     * 
     */
    public SubstanceDispensationEvent createSubstanceDispensationEvent() {
        return new SubstanceDispensationEvent();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.Organization }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.Organization createOrganization() {
        return new vmr.schema.v1_0.vmr.opencds.org.Organization();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.UndeliveredProcedures }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.UndeliveredProcedures createEvaluatedPersonClinicalStatementsUndeliveredProcedures() {
        return new EvaluatedPerson.ClinicalStatements.UndeliveredProcedures();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SupplyProposals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SupplyProposals createEvaluatedPersonClinicalStatementsSupplyProposals() {
        return new EvaluatedPerson.ClinicalStatements.SupplyProposals();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.UndeliveredSubstanceAdministrations }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.UndeliveredSubstanceAdministrations createEvaluatedPersonClinicalStatementsUndeliveredSubstanceAdministrations() {
        return new EvaluatedPerson.ClinicalStatements.UndeliveredSubstanceAdministrations();
    }

    /**
     * Create an instance of {@link EvaluatedPerson }
     * 
     */
    public EvaluatedPerson createEvaluatedPerson() {
        return new EvaluatedPerson();
    }

    /**
     * Create an instance of {@link vmr.schema.v1_0.vmr.opencds.org.AdministrableSubstance }
     * 
     */
    public vmr.schema.v1_0.vmr.opencds.org.AdministrableSubstance createAdministrableSubstance() {
        return new vmr.schema.v1_0.vmr.opencds.org.AdministrableSubstance();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements }
     * 
     */
    public EvaluatedPerson.ClinicalStatements createEvaluatedPersonClinicalStatements() {
        return new EvaluatedPerson.ClinicalStatements();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.DeniedProblems }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.DeniedProblems createEvaluatedPersonClinicalStatementsDeniedProblems() {
        return new EvaluatedPerson.ClinicalStatements.DeniedProblems();
    }

    /**
     * Create an instance of {@link ObservationProposal }
     * 
     */
    public ObservationProposal createObservationProposal() {
        return new ObservationProposal();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SubstanceAdministrationProposals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SubstanceAdministrationProposals createEvaluatedPersonClinicalStatementsSubstanceAdministrationProposals() {
        return new EvaluatedPerson.ClinicalStatements.SubstanceAdministrationProposals();
    }

    /**
     * Create an instance of {@link UndeliveredProcedure }
     * 
     */
    public UndeliveredProcedure createUndeliveredProcedure() {
        return new UndeliveredProcedure();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.Goals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.Goals createEvaluatedPersonClinicalStatementsGoals() {
        return new EvaluatedPerson.ClinicalStatements.Goals();
    }

    /**
     * Create an instance of {@link DeniedAdverseEvent }
     * 
     */
    public DeniedAdverseEvent createDeniedAdverseEvent() {
        return new DeniedAdverseEvent();
    }

    /**
     * Create an instance of {@link AppointmentProposal }
     * 
     */
    public AppointmentProposal createAppointmentProposal() {
        return new AppointmentProposal();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ProcedureEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ProcedureEvents createEvaluatedPersonClinicalStatementsProcedureEvents() {
        return new EvaluatedPerson.ClinicalStatements.ProcedureEvents();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.AppointmentRequests }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.AppointmentRequests createEvaluatedPersonClinicalStatementsAppointmentRequests() {
        return new EvaluatedPerson.ClinicalStatements.AppointmentRequests();
    }

    /**
     * Create an instance of {@link SupplyOrder }
     * 
     */
    public SupplyOrder createSupplyOrder() {
        return new SupplyOrder();
    }

    /**
     * Create an instance of {@link SupplyProposal }
     * 
     */
    public SupplyProposal createSupplyProposal() {
        return new SupplyProposal();
    }

    /**
     * Create an instance of {@link OrganizationSimple }
     * 
     */
    public OrganizationSimple createOrganizationSimple() {
        return new OrganizationSimple();
    }

    /**
     * Create an instance of {@link UndeliveredSubstanceAdministration }
     * 
     */
    public UndeliveredSubstanceAdministration createUndeliveredSubstanceAdministration() {
        return new UndeliveredSubstanceAdministration();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.EntityLists.Organizations }
     * 
     */
    public EvaluatedPerson.EntityLists.Organizations createEvaluatedPersonEntityListsOrganizations() {
        return new EvaluatedPerson.EntityLists.Organizations();
    }

    /**
     * Create an instance of {@link AdverseEvent }
     * 
     */
    public AdverseEvent createAdverseEvent() {
        return new AdverseEvent();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.SubstanceDispensationEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.SubstanceDispensationEvents createEvaluatedPersonClinicalStatementsSubstanceDispensationEvents() {
        return new EvaluatedPerson.ClinicalStatements.SubstanceDispensationEvents();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.UndeliveredSupplies }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.UndeliveredSupplies createEvaluatedPersonClinicalStatementsUndeliveredSupplies() {
        return new EvaluatedPerson.ClinicalStatements.UndeliveredSupplies();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.ProcedureProposals }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.ProcedureProposals createEvaluatedPersonClinicalStatementsProcedureProposals() {
        return new EvaluatedPerson.ClinicalStatements.ProcedureProposals();
    }

    /**
     * Create an instance of {@link RelatedEntity.Entity }
     * 
     */
    public RelatedEntity.Entity createRelatedEntityEntity() {
        return new RelatedEntity.Entity();
    }

    /**
     * Create an instance of {@link EvaluatedPerson.ClinicalStatements.AdverseEvents }
     * 
     */
    public EvaluatedPerson.ClinicalStatements.AdverseEvents createEvaluatedPersonClinicalStatementsAdverseEvents() {
        return new EvaluatedPerson.ClinicalStatements.AdverseEvents();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VMR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "org.opencds.vmr.v1_0.schema.vmr", name = "vmr")
    public JAXBElement<VMR> createVmr(VMR value) {
        return new JAXBElement<VMR>(_Vmr_QNAME, VMR.class, null, value);
    }

}
