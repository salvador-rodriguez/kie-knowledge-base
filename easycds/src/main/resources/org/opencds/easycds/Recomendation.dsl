[then] Recommendation: {message} = vmr.schema.v1_0.vmr.opencds.org.ObjectFactory vmrFactory = new vmr.schema.v1_0.vmr.opencds.org.ObjectFactory();
		VMR vMR = vmrFactory.createVMR();
		
		ObservationResult observationResult = vmrFactory.createObservationResult();
		datatypes.schema.v1_0.vmr.opencds.org.ObjectFactory dataTypesVmrFactory = new datatypes.schema.v1_0.vmr.opencds.org.ObjectFactory();
		
		II ii = dataTypesVmrFactory.createII();
		ii.setRoot("root");
		ii.setExtension("extension");
		observationResult.setId(ii);
		
		CD cd = dataTypesVmrFactory.createCD();
		cd.setCode("code");
		cd.setCodeSystem("codeSystem");
		cd.setCodeSystemName("codeSystemName");
		observationResult.setObservationFocus(cd);
		
		IVLTS ivlts = dataTypesVmrFactory.createIVLTS();
		ivlts.setHigh("highValue");
		ivlts.setLow("lowvalue");
		observationResult.setObservationEventTime(ivlts);
		
		ObservationValue observationValue = vmrFactory.createObservationResultObservationValue();
		CD cdValue = dataTypesVmrFactory.createCD();
		cdValue.setCode("Code");
		cdValue.setCodeSystem("2.16.840.1.113883.3.795.12.1.1");
		cdValue.setCodeSystemName("OpenCDS concepts");
		cdValue.setDisplayName("{message}");
		observationValue.setConcept(cdValue);
		
		EvaluatedPerson evalPerson = vmrFactory.createEvaluatedPerson();
		ClinicalStatements clinicalStatements = vmrFactory.createEvaluatedPersonClinicalStatements();
		
		ObservationResults observationResults = vmrFactory.createEvaluatedPersonClinicalStatementsObservationResults();
		List<ObservationResult> observationResultsList = observationResults.getObservationResult();

		observationResult.setObservationValue(observationValue);
		observationResultsList.add(observationResult);
		
		clinicalStatements.setObservationResults(observationResults);
		evalPerson.setClinicalStatements(clinicalStatements);
		
		vMR.setPatient(evalPerson);
		cdsOutput.setVmrOutput(vMR);